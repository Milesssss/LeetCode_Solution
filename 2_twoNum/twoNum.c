/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
struct ListNode* addTwoNumbers(struct ListNode* l1, struct ListNode* l2) {
    struct ListNode *out=malloc(sizeof(struct ListNode));
    struct ListNode *eout;
    eout = out;
    int flag = 0;
    int sum;
    while(l1!=NULL||l2!=NULL||flag!=0){

        sum = 0;
        if(l1!=NULL){
            sum += l1->val;
            l1=l1->next;
        }
        if(l2!=NULL){
            sum += l2->val;
            l2=l2->next;
        }
        if(flag==1){
            flag = 0;
            sum ++;
        }        
        if(sum>9){
            flag=1;
            eout->val = sum%10;
        }else{
            eout->val = sum;
        }
        if (! l1 && !l2 && !flag) {
            eout->next = NULL;
            break;
        }
        eout->next=malloc(sizeof(struct ListNode));
        eout = eout->next;
    }
        
    return out;
}
